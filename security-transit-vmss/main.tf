provider "azurerm" { 
  version = "=2.0.0"
  features {}
}

locals {
  standard_subnets = {
    management = "10.0.0.0/24",
    public = "10.0.1.0/24",
    private = "10.0.2.0/24"
    }
  subnets = merge(var.subnets, local.standard_subnets)
}

/*
    Add the following line to the resource in this module that depends on the completion of external module components:

    depends_on = ["null_resource.module_depends_on"]

    This will force Terraform to wait until the dependant external resources are created before proceeding with the creation of the
    resource that contains the line above.

    This is a hack until Terraform officially support module depends_on.
*/

variable "module_depends_on" {
  default = [""]
}

resource "null_resource" "module_depends_on" {
  triggers = {
    value = "${length(var.module_depends_on)}"
  }
}


resource "azurerm_resource_group" "transit-rg" {
  name     = var.rg_name
  location = var.location
}

resource "azurerm_virtual_network" "transit-vnet" {
  name                = var.vnet_name 
  location            = azurerm_resource_group.transit-rg.location
  resource_group_name = azurerm_resource_group.transit-rg.name
  address_space       = var.address_space
}

resource "azurerm_subnet" "transit-subnets" {
  for_each = local.subnets
  name                 = each.key
  resource_group_name  = azurerm_resource_group.transit-rg.name
  virtual_network_name = azurerm_virtual_network.transit-vnet.name
  address_prefix       = each.value
}

resource "azurerm_storage_account" "transit-sa" {
  name                     = var.storage_account_name
  resource_group_name      = azurerm_resource_group.transit-rg.name
  location                 = azurerm_resource_group.transit-rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

module "transit-lb" {
  source = "git::https://gitlab.com/public-tf-modules/terraform-azure-loadbalancer?ref=v0.0.2"
  name = "transit_lb"
  resource_group_name = var.rg_name
  location = var.location
  frontend = {
    transit_frontend = {
      subnet_id = azurerm_subnet.transit-subnets["private"].id
      }
  }

  backend_pool_names = ["transit_backend"]
  azlb_probes = {
    transit_probe = {
      port = 22
     }
  }

  azlb_rules = {
    test_lb_rule = {
      protocol =  "All"
      frontend_port = 0
      backend_port = 0
      probe_name = "transit_probe"
      backendpool_name = "transit_backend"
      frontend_name = "transit_frontend"
    }
  }
}

module "transit-bootstrap" {
  source = "git::https://gitlab.com/public-tf-modules/terraform-azure-palo-fw-bootstrap?ref=v0.0.2"
  name = "transit-environtment-storage"
  storage_account_name = azurerm_storage_account.transit-sa.name
  storage_account_key  = azurerm_storage_account.transit-sa.primary_access_key
}

module "fw-ss" {
  source = "git::https://gitlab.com/public-tf-modules/terraform-azure-panfw-vmss?ref=v0.0.5"
  location = var.location
  ssh_key = var.ssh_key
  resource_group_name = azurerm_resource_group.transit-rg.name
  computer_name_prefix = var.computer_name_prefix 
  scale_set_name = var.scale_set_name
  bootstrap = true
  bs_storage_account = azurerm_storage_account.transit-sa.name
  bs_access_key = azurerm_storage_account.transit-sa.primary_access_key
  file_share = module.transit-bootstrap.storage_share_name
  app_insights = true
  interfaces = [
      {
      name = "management" 
      index = 1
      enable_ip_forwarding = false
      subnet_id = azurerm_subnet.transit-subnets["management"].id
      },
      {
      index = 2
      name = "private" 
      subnet_id = azurerm_subnet.transit-subnets["private"].id 
      enable_ip_forwarding = true
      enable_accelerated_networking = true
      load_balancer_backend_address_pool_ids = [module.transit-lb.backend_pools["transit_backend"].id]
     },
     {
      name = "public" 
      index = 0
      primary = true
      subnet_id = azurerm_subnet.transit-subnets["public"].id 
      enable_ip_forwarding = true
      enable_accelerated_networking = true
      }
  ]
  email_notify     = var.email_notify
  vmss_default_cap = 2
  vmss_min_cap     = 2
  vmss_max_cap     = 4
  scale_set_rules = {
    "Percentage CPU" = {
      scale_out_threshold = 5,
      scale_in_threshold  = 20
    }
  }
}


