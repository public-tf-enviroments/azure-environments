variable "ssh_key"{}
variable "location" {}
variable "vnet_name" {}
variable "rg_name"{}
variable "subnets" {
  default = {}
}  
variable "address_space" {}
variable "computer_name_prefix" {}
variable "scale_set_name" {}
variable "email_notify" {}
variable "storage_account_name" {}
