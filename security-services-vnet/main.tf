provider "azurerm" {
  version = "=2.0.0"
  features {}
}

locals {
  subnets = merge(var.subnets, {})
}

resource "azurerm_resource_group" "security-services-rg" {
  name     = var.rg_name
  location = var.location
}

resource "azurerm_virtual_network" "security-services-vnet" {
  name                = var.vnet_name
  location            = azurerm_resource_group.security-services-rg.location
  resource_group_name = azurerm_resource_group.security-services-rg.name
  address_space       =  var.address_space
}

resource "azurerm_subnet" "security-services-subnets" {
  for_each = local.subnets
  name                 = each.key
  resource_group_name  = azurerm_resource_group.security-services-rg.name
  virtual_network_name = azurerm_virtual_network.security-services-vnet.name
  address_prefix       = each.value
}

resource "azurerm_network_security_group" "panorama-security-group-interface" {
  name                = "panorama-nsg-interface"
  location            = azurerm_resource_group.security-services-rg.location
  resource_group_name = azurerm_resource_group.security-services-rg.name
}

resource "azurerm_network_security_group" "panorama-security-group-subnet" {
  name                = "panorama-nsg-subnet"
  location            = azurerm_resource_group.security-services-rg.location
  resource_group_name = azurerm_resource_group.security-services-rg.name
}

resource "azurerm_subnet_network_security_group_association" "panoarma-nsg-association" {
  subnet_id                 = azurerm_subnet.security-services-subnets["panorama"].id
  network_security_group_id = azurerm_network_security_group.panorama-security-group-subnet.id
} 

resource "azurerm_public_ip" "panorama_pip" {
  for_each = toset(var.panorama_pips)
  name                = each.key
  location            = var.location
  resource_group_name = azurerm_resource_group.security-services-rg.name
  allocation_method   = "Static"
} 

resource "azurerm_network_security_rule" "panorama-allow_all-rule" {
  name                       = "allow_ all"
  priority                   = 100
  direction                  = "Inbound"
  access                     = "Allow"
  protocol                   = "Tcp"
  source_port_range          = "*"
  destination_port_range     = "*"
  source_address_prefix      = "*"
  destination_address_prefix = "*"
  resource_group_name         = var.rg_name
  network_security_group_name = azurerm_network_security_group.panorama-security-group-subnet.name 
}

resource "azurerm_network_security_rule" "panorama-allow_my-ip-rule" {
  name                       = "allow_my_ip "
  priority                   = 100
  direction                  = "Inbound"
  access                     = "Allow"
  protocol                   = "Tcp"
  source_port_range          = "*"
  destination_port_range     = "*"
  source_address_prefix      = var.local_ip 
  destination_address_prefix = "*"
  resource_group_name         = var.rg_name
  network_security_group_name = azurerm_network_security_group.panorama-security-group-interface.name 
}

module "panorama" {
  source = "git::https://gitlab.com/public-tf-modules/terraform-azure-panorama?ref=v0.1.0"
  location = var.location
  resource_group_name = azurerm_resource_group.security-services-rg.name
  panoramas = {
    ha-panorama1 = {
      admin_username = "fwadmin"
      ssh_key = var.ssh_key
      pan_size = "Standard_DS5_v2"
      public_ip_id = azurerm_public_ip.panorama_pip["panorama-1"].id
      subnet_id = azurerm_subnet.security-services-subnets["panorama"].id
      network_security_group_id = azurerm_network_security_group.panorama-security-group-interface.id
      logger_size = 2048
      },
    ha-panorama2 = {
      admin_username = "fwadmin"
      ssh_key = var.ssh_key
      pan_size = "Standard_DS5_v2"
      public_ip_id = azurerm_public_ip.panorama_pip["panorama-2"].id
      subnet_id = azurerm_subnet.security-services-subnets["panorama"].id
      network_security_group_id = azurerm_network_security_group.panorama-security-group-interface.id
      logger_size = 2048
     }
  }
}
